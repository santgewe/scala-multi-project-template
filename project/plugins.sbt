ThisBuild / useSuperShell := true
ThisBuild / autoStartServer := false

resolvers += Resolver.sonatypeRepo("releases")
resolvers += Resolver.sonatypeRepo("snapshots")
//SonaType Publishing
addSbtPlugin("org.xerial.sbt" % "sbt-sonatype" % "2.3")
addSbtPlugin("com.jsuereth" % "sbt-pgp" % "1.1.1")

//Docker
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.3")
//Assembly
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.5")

addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.5.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "1.0.0")
// Needed for importing the project into Eclipse
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "5.2.4")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")

addSbtPlugin(
  //"io.get-coursier" % "sbt-coursier" % coursier.util.Properties.version
  "io.get-coursier" % "sbt-coursier" % "1.1.0-M6"
)

addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.11")

addSbtPlugin("pl.project13.scala" % "sbt-jmh" % "0.3.7")
