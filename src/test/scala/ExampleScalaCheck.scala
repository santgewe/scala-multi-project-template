import org.scalacheck.{ Prop, Properties }
import org.typelevel.claimant.Claim

object ExampleScalaCheck extends Properties("MyRootTest") {
  property("float is associative") = Prop.forAll {
    (x: Float, y: Float, z: Float) => Claim((x + (y + z)) == ((x + y) + z))
  }
}

object AnotherTest extends Properties("AnotherRootTest") {
  property("ints have distinct inverses") = {
    Prop.forAll((n: Int) => Claim(n == 0 || n != -n))
  }
}
