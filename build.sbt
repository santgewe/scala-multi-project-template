//Imports
import Settings._
import Dependencies._
import Docker._
import ModuleNames._

ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-language:_",
  "-unchecked",
  "-language:postfixOps",
  "-Yrangepos"
)
addCompilerPlugin(
  "org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full
)
addCommandAlias("fix", "all compile:scalafix test:scalafix")

//Sbt Log Level
logLevel := Level.Info

//Add all the command alias's
CommandAlias.allCommandAlias

lazy val scalaprojecttemplate = (project in file("."))
  .settings(rootSettings: _*)
  .settings(
    libraryDependencies ++= rootDependencies,
    libraryDependencies ++= testDependencies
  )
  //.enablePlugins(Artifactory)
  //.enablePlugins(SonaType)
  .settings(
    addCompilerPlugin(scalafixSemanticdb), // enable SemanticDB
    scalacOptions ++= List(
      "-Yrangepos", // required by SemanticDB compiler plugin
      "-Ywarn-unused" // required by `RemoveUnused` rule
    ),
    Compile / console / scalacOptions --= Seq(
      "-Xfatal-warnings"
    ),
    Test / console / scalacOptions :=
      (Compile / console / scalacOptions).value
  )
  .settings(rootDockerSettings)
  .enablePlugins(AshScriptPlugin)
  .enablePlugins(AssemblyPlugin)
  .enablePlugins(JmhPlugin)
  .aggregate(moduleatemplate, modulebtemplate)

lazy val moduleatemplate = (project in file(moduleAKey))
  .settings(moduleASettings: _*)
  .settings(
    libraryDependencies ++= moduleADependencies,
    libraryDependencies ++= testDependencies
  )
  .settings(
    addCompilerPlugin(scalafixSemanticdb), // enable SemanticDB
    scalacOptions ++= List(
      "-Yrangepos", // required by SemanticDB compiler plugin
      "-Ywarn-unused" // required by `RemoveUnused` rule
    ),
    Compile / console / scalacOptions --= Seq(
      "-Xfatal-warnings"
    ),
    Test / console / scalacOptions :=
      (Compile / console / scalacOptions).value
  )
  //.enablePlugins(Artifactory)
  //.enablePlugins(SonaType)
  .settings(moduleADockerSettings)
  .enablePlugins(AshScriptPlugin)
  .enablePlugins(AssemblyPlugin)

lazy val modulebtemplate = (project in file(moduleBKey))
  .settings(moduleBSettings: _*)
  .settings(
    libraryDependencies ++= moduleBDependencies,
    libraryDependencies ++= testDependencies
  )
  .settings(
    addCompilerPlugin(scalafixSemanticdb), // enable SemanticDB
    scalacOptions ++= List(
      "-Yrangepos", // required by SemanticDB compiler plugin
      "-Ywarn-unused" // required by `RemoveUnused` rule
    ),
    Compile / console / scalacOptions --= Seq(
      "-Xfatal-warnings"
    ),
    Test / console / scalacOptions :=
      (Compile / console / scalacOptions).value
  )
  //.enablePlugins(Artifactory)
  //.enablePlugins(SonaType)
  .settings(moduleBDockerSettings)
  .enablePlugins(AshScriptPlugin)
  .enablePlugins(AssemblyPlugin)
